const requirePackage = require('./import');
const express = requirePackage.packages.express;
const bodyParser = requirePackage.packages.bodyParser;
// const mongoose = requirePackage.packages.mongoose;
const http = require('http');
const socket = requirePackage.packages.socket;

// const db = require('./db').db;

const socketEvents = require('./socketEvents');

const app = express();
// const options = {
//   key: fs.readFileSync(__dirname + '/bin/private.key'),
//   cert: fs.readFileSync(__dirname + '/bin/certificate.pem')
// };

const server = http.Server(app);
const io = socket(server, {
  pingTimeout: 8000,
  cookie: false
});

app.io = io;

app.use(express.static(__dirname + '/public'));     
// app.use(express.static(path.join(__dirname + '/uploads')));                 // set the static files location /public/img will be /img for users

// app.use(express.static(path.join(__dirname + '/uploads', '/gaurav@gmail.com', '/stories'))); 
// app.use(express.static('damroo-front-end/build'));
// app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({ 'extended': 'true' }));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json({limit: '50mb'}));                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
// app.use(methodOverride());

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

socketEvents(io);

const PORT = process.env.PORT || 3000;

server.listen(PORT, () => {
  console.log(`listening on *:${PORT}`);
});