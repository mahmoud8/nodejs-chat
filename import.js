var express = require('express');                    // create our app w/ express                    // mongoose for mongodb
var morgan = require('morgan');                    // log requests to the console (express4)
var bodyParser = require('body-parser');          // pull information from HTML POST (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)

// var mongoose = require('mongoose');
const socket = require('socket.io');

var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "jetjash",
  database: "marvel_app"
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
  // var sql = "CREATE TABLE customers (name VARCHAR(255), address VARCHAR(255))";
  // con.query(sql, function (err, result) {
  //   if (err) throw err;
  //   console.log("Table created");
  // });
});


// mongoose.connect('mongodb://localhost/marvel_app', {
//   useMongoClient: true
// });

// var db = mongoose.connection;
// db.on('error', console.error.bind(console, 'db connection error:'));
// db.once('open', function callback() {
//   console.log("db Connection Successful");
// });

module.exports.packages = {express, morgan, bodyParser, methodOverride,
  socket, con
};